<?php
/**
 * WHMCS BEZA Site Checker
 *
 * Use this addon to use Google Safebrowsing API, Phishtank API and Sucuri
 *
 * @package    WHMCS 5.2.1+
 * @author     BEZA
 * @copyright  Copyright (c) BEZA 2017-2018
 * @license    GPL v3+
 * @version    1.0.1
 * @link       BEZA.NET
 */

if (!defined("WHMCS"))
    die("This file cannot be accessed directly");

define( 'BSC_ROOT', dirname( __FILE__ ) );

require_once( BSC_ROOT . '/functions.php');
require_once( BSC_ROOT . '/inc/whmcse.php' );
if (!class_exists("PHPMailer")){
    require('PHPMailer/PHPMailerAutoload.php');
}  

function bezasitechecker_config() {
    $configarray = array(
    "name" => "BEZA Site Checker",
    "description" => "This addon checks against Google Safe Browsing, Sucuri, Phishtank whether the url is blacklisted",
    "version" => "1.0.1",
    "author" => "BEZA",
    "language" => "english",
    "fields" => array(
        "enable_google" => array ("FriendlyName" => "Use Google Safebrowsing", "Type" => "yesno", "Size" => "25", "Description" => "Enable to use Google Safebrowsing", ),
		"enable_phishtank" => array ("FriendlyName" => "Use Phishtank", "Type" => "yesno", "Size" => "25", "Description" => "Enable to use Phishtank", ),
		"enable_sucuri" => array ("FriendlyName" => "Use Sucuri", "Type" => "yesno", "Size" => "25", "Description" => "Enable to use Sucuri", ),
		"enable_virustotal" => array ("FriendlyName" => "Use Virustotal", "Type" => "yesno", "Size" => "25", "Description" => "Enable to use Virustotal", )
    //    "userkey" => array ("FriendlyName" => "User Key", "Type" => "text", "Size" => "75", "Description" => "User Key from <a href=\"http://pushover.net\" target=\"_blank\"> Pushover</a>.", "Default" => "", )
    ));
    return $configarray;
}

function bezasitechecker_activate() {

    # Create Custom DB Table
    $query = "CREATE TABLE `mod_bezasitechecker` (`id` INT( 1 ) NOT NULL AUTO_INCREMENT PRIMARY KEY ,`bezasitechecker` TEXT NOT NULL )";
    $result = full_query($query);

    # Return Result
    return array('status'=>'success','description'=>'Great, everything is activated. ');

}

function bezasitechecker_deactivate() {

    # Remove Custom DB Table
    $query = "DROP TABLE `mod_bezasitechecker`";
    $result = full_query($query);

    # Return Result
    return array('status'=>'success','description'=>'WHMCS BEZA Site Checker Deactivated.');

}
function bezasitechecker_output() {
	echo '<div class="panel panel-default">
	  <div class="panel-heading">Google Safebrowsing</div>
	  <div class="panel-body">
		<p>Check the urls through Google Safebrowsing API</p>
		<form method="POST" class="form-inline"><div class="form-group"><input type="hidden" class="beza-input form-control" name="checkurl" value="1"></div><input type="submit" class="btn btn-primary" value="Check URL"></form>
	  </div>
	</div>';
	echo '<div class="panel panel-default">
	  <div class="panel-heading">Phishtank</div>
	  <div class="panel-body">
		<p>Check the urls through Phishtank API</p>
		<form method="POST" class="form-inline"><div class="form-group"><input type="hidden" class="beza-input form-control" name="checkurl" value="2"></div><input type="submit" class="btn btn-primary" value="Check URL"></form>
	  </div>
	</div>';
	echo '<div class="panel panel-default">
	  <div class="panel-heading">Google Safebrowsing using GetClientsDomains</div>
	  <div class="panel-body">
		<p>Check the urls through Google Safebrowsing using GetClientsDomains. NO EMAIL SENDING YET</p>
		<form method="POST" class="form-inline"><div class="form-group"><input type="hidden" class="beza-input form-control" name="checkurl" value="3"></div><input type="submit" class="btn btn-primary" value="Check URL"></form>
	  </div>
	</div>';
	//Google Safebrowsing is 1 - Make sure to edit the $data on function sendPHPMailGoogle
	if( isset($_POST['checkurl']) && $_POST['checkurl'] == 1){
		sendPHPMailGoogle();
	}
	if( isset($_POST['checkurl']) && $_POST['checkurl'] == 2){
		sendPHPMailPhishTank();
	}
	if( isset($_POST['checkurl']) && $_POST['checkurl'] == 3){
		sendwithBuffer();
		//$check_google = bsc_get_enable_google();
		//var_dump($check_google);
	}
}
function sendwithBuffer(){
	
	//Virustotal
	
	/* $virustotalSiteCheck = virustotalSiteCheck( 'google.com' );
	if( $virustotalSiteCheck['positives'] == 0 ){
		echo 'hello';
	} */
	
	$command = 'GetClientsDomains';
	$postData = array(
		'limitnum' => 999,
		'stats' => 'true',
		
	);
	$adminUsername = 'BEZA-Dev-Team-3251';

	$results = localAPI($command, $postData, $adminUsername);
	foreach($results["domains"]["domain"] as $items ){
		$message = "";
		$error = 0;
		
		//Check which are enabled
		$check_google = bsc_get_enable_google();
		$check_phishtank = bsc_get_enable_phishtank();
		$check_sucuri = bsc_get_enable_sucuri();
		$check_virustotal = bsc_get_enable_virustotal();
		
		//Google Safe Browsing
		if ($check_google == 'on'){
			$siteCheck = googleSiteCheck( $items["domainname"] );
			if( isset($siteCheck['matches'][0]['threat']['url']) ){
				//echo $items["domainname"] . ' - not safe ' . $items['userid'] . "<br/>";
				$message .= "Google Safe Browsing Result : " .$items["domainname"] . " is blacklisted. <br/>";
				$error = 1;
				
			}else{
				$message .= "Google Safe Browsing Result : " .$items["domainname"] . " is not blacklisted. <br/>";
			}
		}
		
		//Phishtank
		if ($check_phishtank == 'on'){
			$phishtankCheck = phishtankSiteCheck( $items["domainname"] );
			if( isset($phishtankCheck['results']['url']) && $phishtankCheck['results']['in_database'] == true ){
				$message .= "Phishtank Result : " .$items["domainname"] . " is blacklisted. <br/>";
				$error = 1;
			}else{
				$message .= "Phishtank Result : " .$items["domainname"] . " is not blacklisted. <br/>";
			}
		}
		
		//Sucuri
		
		if ($check_sucuri == 'on'){
			$sucuriCheck = sucuriSiteCheck( $items["domainname"] );
			if( $sucuriCheck != "" ){
				$message .= "Sucuri Result : " .$items["domainname"] . " is blacklisted. <br/>";
				$error = 1;
			}else{
				$message .= "Sucuri Result : " .$items["domainname"] . " is not blacklisted. <br/>";
			}
		}
		
		//Virustotal
		if ($check_virustotal == 'on'){
			$virustotalSiteCheck = virustotalSiteCheck( $items["domainname"] );
			if( $virustotalSiteCheck['positives'] == 0 ){
				$message .= "Virustotal Result : " .$items["domainname"] . " is not blacklisted. <br/>";
			}else{
				$message .= "Virustotal Result : " .$items["domainname"] . " is blacklisted. <br/>";
				$error = 1;
			}
		}
		
		if( $error == 1){
			//SENDEMAIL using Local API
			$e_command = 'SendEmail';
			$e_postData = array(
				'customtype' => 'general',
				'custommessage' => $message,
				'customsubject' => 'BEZA Blacklisting Report',
				'id' => (string)$items['userid'],
			);
			
			$e_results = localAPI($e_command, $e_postData, $adminUsername);
			echo "mail sending - "; print_r($e_results) . "<br/>";
		}		
	}
}

function sendPHPMailGoogle(){
	$data = "http://ianfette.org,reyesmarkdaryl@gmail.com;http://facebook.com,reyesmarkdaryl@gmail.com";
	$parsedData = explode(";", $data);
	foreach($parsedData as $line ){
		$parsedLine = explode(",", $line);
		$siteCheck = siteCheck( $parsedLine[0] );
		if( isset($siteCheck['matches'][0]['threat']['url']) ){
			//Create a new PHPMailer instance
			$mail = new PHPMailer;
			// Set PHPMailer to use the sendmail transport
			$mail->isSendmail();
			//Set who the message is to be sent from
			$mail->setFrom('no-reply@beza.net', 'BEZA');
			//Set an alternative reply-to address
			$mail->AddReplyTo( 'no-reply@beza.net', 'BEZA' );
			//Set who the message is to be sent to
			$mail->addAddress($parsedLine[1]);
			//Set the subject line
			$mail->Subject = 'Google Safebrowsing Result';
			$mail->IsHTML( true );
			$mail->Body    = "The url ".$parsedLine[0]." is blacklisted on Google Safebrowsing";
			//send the message, check for errors
			if (!$mail->send()) {
				echo "Mailer Error: " . $mail->ErrorInfo . " at ". $parsedLine[0];
			} else {
				echo $parsedLine[0] . ' - not safe. Message was sent<br/>';
			}
		}else{
			echo $parsedLine[0] . ' - safe<br/>';
		}
	}
	
}
function sendPHPMailPhishTank(){
	$data = "http://ianfette.org,reyesmarkdaryl@gmail.com;http://facebook.com,reyesmarkdaryl@gmail.com;http://nakinaindustries.com,reyesmarkdaryl@gmail.com";
	$parsedData = explode(";", $data);
	foreach($parsedData as $line ){
		$parsedLine = explode(",", $line);
		$phishtankCheck = phishtankCheck( $parsedLine[0] );
		if( isset($phishtankCheck['results']['url']) && $phishtankCheck['results']['in_database'] == true ){
			//Create a new PHPMailer instance
			$mail = new PHPMailer;
			// Set PHPMailer to use the sendmail transport
			$mail->isSendmail();
			//Set who the message is to be sent from
			$mail->setFrom('no-reply@beza.net', 'BEZA');
			//Set an alternative reply-to address
			$mail->AddReplyTo( 'no-reply@beza.net', 'BEZA' );
			//Set who the message is to be sent to
			$mail->addAddress($parsedLine[1]);
			//Set the subject line
			$mail->Subject = 'Phishtank Result';
			$mail->IsHTML( true );
			$url = $parsedLine[0];
			$mail->Body = "The url ". $url ." is blacklisted on Phishtank";
			//send the message, check for errors
			if (!$mail->send()) {
				echo "Mailer Error: " . $mail->ErrorInfo . " at ". $parsedLine[0];
			} else {
				echo $parsedLine[0] . ' - not safe. Message was sent<br/>';
			}
		}else{
			echo $parsedLine[0] . ' - safe<br/>';
		}
	}
	
}

function bezasitechecker_sidebar($vars) {

    $modulelink = $vars['modulelink'];
    $version = $vars['version'];

    $sidebar = '<span class="header"><img src="images/icons/addonmodules.png" class="absmiddle" width="16" height="16" /> BEZA Site Checker</span>
	<ul class="menu">
        <li><a href="#">Updates and Releases</a></li>
        <li><a href="#">Version: '.$version.'</a></li>
    </ul>';
    return $sidebar;

}

function googleSiteCheck( $url ){
	error_reporting( 0 );

	$api_url = "https://safebrowsing.googleapis.com/v4/threatMatches:find?key=AIzaSyB330104r1vKfGGOC5gXQIO7ZoJ3YIj63w";

	$data = new stdClass();
	$data->client->clientId = "Firisgo";
	$data->client->clientVersion = "1.5.2";
	@$data->threatInfo->threatTypes = array( "MALWARE", "SOCIAL_ENGINEERING" );
	@$data->threatInfo->platformTypes = array( "WINDOWS" );
	@$data->threatInfo->threatEntryTypes = array( "URL" );
	@$data->threatInfo->threatEntries = array(
	  array( "url" => $url )  
	);

	$curl = curl_init();    
	curl_setopt( $curl, CURLOPT_URL, $api_url ); 
	curl_setopt( $curl, CURLOPT_RETURNTRANSFER, TRUE );
	curl_setopt( $curl, CURLOPT_HTTPHEADER, array('Content-Type: application/json') );
	curl_setopt( $curl, CURLOPT_POST, TRUE );
	curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, FALSE );
	curl_setopt( $curl, CURLOPT_POSTFIELDS, json_encode( $data ) );


	$curl_response = curl_exec($curl);
	curl_close($curl);
	$response = json_decode($curl_response , true);
	return $response ;
}

function phishtankSiteCheck( $url ) {
	error_reporting( 0 );

	$api_key = "b521a5145fe5efa4201e7210b4ec5b416ba71724784fb2eb3d33a08f92b58b8b";
	$api_url = "http://checkurl.phishtank.com/checkurl/";
	//$url =  base64_encode("http://nakinaindustries.com");

	$curl = curl_init();    
	curl_setopt( $curl, CURLOPT_URL, $api_url ); 
	curl_setopt( $curl, CURLOPT_RETURNTRANSFER, TRUE );
	curl_setopt( $curl, CURLOPT_CUSTOMREQUEST, 'POST' );
	curl_setopt( $curl, CURLOPT_POSTFIELDS, array(
		"url" => base64_encode( $url ),
		"format" => 'json',
		"app_key" => $api_key
	));


	$curl_response = curl_exec($curl);
	curl_close($curl);

	//echo $curl_response; 
	$response = json_decode($curl_response , true);
	return $response ;
}

function sucurisitecurl($url) {
	// Assigning cURL options to an array
	$options = Array(
		CURLOPT_RETURNTRANSFER => TRUE,  // Setting cURL's option to return the webpage data
		CURLOPT_FOLLOWLOCATION => TRUE,  // Setting cURL to follow 'location' HTTP headers
		CURLOPT_AUTOREFERER => TRUE, // Automatically set the referer where following 'location' HTTP headers
		CURLOPT_CONNECTTIMEOUT => 120,   // Setting the amount of time (in seconds) before the request times out
		CURLOPT_TIMEOUT => 120,  // Setting the maximum amount of time for cURL to execute queries
		CURLOPT_MAXREDIRS => 10, // Setting the maximum number of redirections to follow
		CURLOPT_USERAGENT => $_SERVER['HTTP_USER_AGENT'], //"Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.9.1a2pre) Gecko/2008073000 Shredder/3.0a2pre ThunderBrowse/3.2.1.8",  // Setting the useragent
		CURLOPT_URL => $url, // Setting cURL's URL option with the $url variable passed into the function
		CURLOPT_SSL_VERIFYPEER => FALSE
	);
	 
	$ch = curl_init();  // Initialising cURL 
	curl_setopt_array($ch, $options);   // Setting cURL's options using the previously assigned array data in $options
	$data = curl_exec($ch); // Executing the cURL request and assigning the returned data to the $data variable
	curl_close($ch);    // Closing cURL 
	return $data;   // Returning the data from the function 
}

function site_scrape_between($data, $start, $end){
	$data = stristr($data, $start); // Stripping all data from before $start
	$data = substr($data, strlen($start));  // Stripping $start
	$stop = stripos($data, $end);   // Getting the position of the $end of the data to scrape
	$data = substr($data, 0, $stop);    // Stripping all data from after and including the $end of the data to scrape
	return $data;   // Returning the scraped data from the function
}

function sucuriSiteCheck($url){
	
	//$theUrl = "https://sitecheck.sucuri.net/results/goooogleadsence.biz";
	$theUrl = "https://sitecheck.sucuri.net/results/" . $url;
	$results_page = sucurisitecurl($theUrl); // Downloading the results page using our curl() funtion
     
    $results_page = site_scrape_between($results_page, "<table class=\"table main-result\">", "</table>"); // Scraping out only the middle section of the results page that contains our results
     
    $separate_results = explode("<tr>", $results_page);   // Expploding the results into separate parts into an array
         
	$status = site_scrape_between($separate_results[3], "<td class=\"red\">", "</td>");
	

	return $status;
	
}
function virustotalSiteCheck($url){
	$api_key = getenv('VT_API_KEY') ? getenv('VT_API_KEY') :'80eb4a2c6fc02ca6194349f6add36b6ee8771febe8de6b9792062d6bc1d0f495';
	//$scan_url = 'goooogleadsence.biz';
	 
	//$post = array('apikey' => $api_key,'resource'=> $scan_url, 'scan' => 1 );
	$post = array('apikey' => $api_key,'resource'=> $url, 'scan' => 1 );
	
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, 'https://www.virustotal.com/vtapi/v2/url/report');
	curl_setopt($ch, CURLOPT_POST, True);
	curl_setopt($ch, CURLOPT_VERBOSE, 1); // remove this if your not debugging
	curl_setopt($ch, CURLOPT_ENCODING, 'gzip,deflate'); // please compress data
	curl_setopt($ch, CURLOPT_USERAGENT, "gzip, My php curl client");
	curl_setopt($ch, CURLOPT_RETURNTRANSFER ,True);
	curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
	 
	$result=curl_exec ($ch);
	$status_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
	//print("status = $status_code\n");
	
	if ($status_code == 200) { // OK
	  $js = json_decode($result, true);
	  
		return $js;
		
	} else {  // Error occured

	  return $result;
	}
	curl_close ($ch);
}

