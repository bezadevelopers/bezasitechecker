<?php
/**
 * WHMCS BEZA Site Checker
 *
 * Use this addon to use Google Safebrowsing API, Phishtank API and Sucuri
 *
 * @package    WHMCS 5.2.1+
 * @author     BEZA
 * @copyright  Copyright (c) BEZA 2017-2018
 * @license    GPL v3+
 * @version    1.0.1
 * @link       BEZA.NET
 */

if ( ! defined( "WHMCS" ) ) die( "This file cannot be accessed directly" );

define( 'BSC_ROOT', dirname( __FILE__ ) );

require_once( BSC_ROOT . '/functions.php' );
require_once( BSC_ROOT . '/inc/whmcse.php' );

function bezasitechecker_DailyCronJob (){
	$command = 'GetClientsDomains';
	$postData = array(
		'limitnum' => 999,
		'stats' => 'true',
		
	);
	$adminUsername = 'BEZA-Dev-Team-3251';

	$results = localAPI($command, $postData, $adminUsername);
	foreach($results["domains"]["domain"] as $items ){
		$message = "DAILY CRON <br/>";
		$error = 0;
		
		//Check which are enabled
		$check_google = bsc_get_enable_google();
		$check_phishtank = bsc_get_enable_phishtank();
		$check_sucuri = bsc_get_enable_sucuri();
		
		//Google Safe Browsing
		if ($check_google == 'on'){
			$siteCheck = siteCheck( $items["domainname"] );
			if( isset($siteCheck['matches'][0]['threat']['url']) ){
				//echo $items["domainname"] . ' - not safe ' . $items['userid'] . "<br/>";
				$message .= "Google Safe Browsing Result : " .$items["domainname"] . " is blacklisted. <br/>";
				$error = 1;
				
			}else{
				$message .= "Google Safe Browsing Result : " .$items["domainname"] . " is not blacklisted. <br/>";
			}
		}
		
		//Phishtank
		if ($check_phishtank == 'on'){
			$phishtankCheck = phishtankCheck( $items["domainname"] );
			if( isset($phishtankCheck['results']['url']) && $phishtankCheck['results']['in_database'] == true ){
				$message .= "Phishtank Result : " .$items["domainname"] . " is blacklisted. <br/>";
				$error = 1;
			}else{
				$message .= "Phishtank Result : " .$items["domainname"] . " is not blacklisted. <br/>";
			}
		}
		
		//Sucuri
		if ($check_sucuri == 'on'){
			$sucuriCheck = sucuriCheck( $items["domainname"] );
			if( $sucuriCheck != "" ){
				$message .= "Sucuri Result : " .$items["domainname"] . " is blacklisted. <br/>";
				$error = 1;
			}else{
				$message .= "Sucuri Result : " .$items["domainname"] . " is not blacklisted. <br/>";
			}
		}
		//Virustotal
		if ($check_virustotal == 'on'){
			$virustotalCheck = virustotalCheck( $items["domainname"] );
			if( $virustotalCheck['positives'] == 0 ){
				$message .= "Virustotal Result : " .$items["domainname"] . " is not blacklisted. <br/>";
			}else{
				$message .= "Virustotal Result : " .$items["domainname"] . " is blacklisted. <br/>";
				$error = 1;
			}
		}
		
		if( $error == 1){
			//SENDEMAIL using Local API
			$e_command = 'SendEmail';
			$e_postData = array(
				'customtype' => 'general',
				'custommessage' => $message,
				'customsubject' => 'BEZA Blacklisting Report',
				'id' => (string)$items['userid'],
			);
			
			$e_results = localAPI($e_command, $e_postData, $adminUsername);
			logModuleCall( 'bezasitechecker', 'hook_DailyCronJob', $postData, $e_results );
		}		
	}
}

function bezasitechecker_PostAutomationTask (){
	$command = 'GetClientsDomains';
	$postData = array(
		'limitnum' => 999,
		'stats' => 'true',
		
	);
	$adminUsername = 'BEZA-Dev-Team-3251';

	$results = localAPI($command, $postData, $adminUsername);
	foreach($results["domains"]["domain"] as $items ){
		$message = "PostAutomationTask CRON <br/>";
		$error = 0;
		//Check which are enabled
		$check_google = bsc_get_enable_google();
		$check_phishtank = bsc_get_enable_phishtank();
		$check_sucuri = bsc_get_enable_sucuri();
		
		
		//Google Safe Browsing
		if ($check_google == 'on'){
			$siteCheck = siteCheck( $items["domainname"] );
			if( isset($siteCheck['matches'][0]['threat']['url']) ){
				//echo $items["domainname"] . ' - not safe ' . $items['userid'] . "<br/>";
				$message .= "Google Safe Browsing Result : " .$items["domainname"] . " is blacklisted. <br/>";
				$error = 1;
				
			}else{
				$message .= "Google Safe Browsing Result : " .$items["domainname"] . " is not blacklisted. <br/>";
			}
		}
		
		
		//Phishtank
		if ($check_phishtank == 'on'){
			$phishtankCheck = phishtankCheck( $items["domainname"] );
			if( isset($phishtankCheck['results']['url']) && $phishtankCheck['results']['in_database'] == true ){
				$message .= "Phishtank Result : " .$items["domainname"] . " is blacklisted. <br/>";
				$error = 1;
			}else{
				$message .= "Phishtank Result : " .$items["domainname"] . " is not blacklisted. <br/>";
			}
		}
		
		//Sucuri
		if ($check_sucuri == 'on'){
			$sucuriCheck = sucuriCheck( $items["domainname"] );
			if( $sucuriCheck != "" ){
				$message .= "Sucuri Result : " .$items["domainname"] . " is blacklisted. <br/>";
				$error = 1;
			}else{
				$message .= "Sucuri Result : " .$items["domainname"] . " is not blacklisted. <br/>";
			}
		}
		
		//Virustotal
		if ($check_virustotal == 'on'){
			$virustotalCheck = virustotalCheck( $items["domainname"] );
			if( $virustotalCheck['positives'] == 0 ){
				$message .= "Virustotal Result : " .$items["domainname"] . " is not blacklisted. <br/>";
			}else{
				$message .= "Virustotal Result : " .$items["domainname"] . " is blacklisted. <br/>";
				$error = 1;
			}
		}
		
		if( $error == 1){
			//SENDEMAIL using Local API
			$e_command = 'SendEmail';
			$e_postData = array(
				'customtype' => 'general',
				'custommessage' => $message,
				'customsubject' => 'BEZA Blacklisting Report',
				'id' => (string)$items['userid'],
			);
			
			$e_results = localAPI($e_command, $e_postData, $adminUsername);
			logModuleCall( 'bezasitechecker', 'hook_DailyCronJob', $postData, $e_results );
		}		
	}
}

function siteCheck( $url ){
	error_reporting( 0 );

	$api_url = "https://safebrowsing.googleapis.com/v4/threatMatches:find?key=AIzaSyB330104r1vKfGGOC5gXQIO7ZoJ3YIj63w";

	$data = new stdClass();
	$data->client->clientId = "Firisgo";
	$data->client->clientVersion = "1.5.2";
	@$data->threatInfo->threatTypes = array( "MALWARE", "SOCIAL_ENGINEERING" );
	@$data->threatInfo->platformTypes = array( "WINDOWS" );
	@$data->threatInfo->threatEntryTypes = array( "URL" );
	@$data->threatInfo->threatEntries = array(
	  array( "url" => $url )  
	);

	$curl = curl_init();    
	curl_setopt( $curl, CURLOPT_URL, $api_url ); 
	curl_setopt( $curl, CURLOPT_RETURNTRANSFER, TRUE );
	curl_setopt( $curl, CURLOPT_HTTPHEADER, array('Content-Type: application/json') );
	curl_setopt( $curl, CURLOPT_POST, TRUE );
	curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, FALSE );
	curl_setopt( $curl, CURLOPT_POSTFIELDS, json_encode( $data ) );


	$curl_response = curl_exec($curl);
	curl_close($curl);
	$response = json_decode($curl_response , true);
	return $response ;
}

function phishtankCheck( $url ) {
	error_reporting( 0 );

	$api_key = "b521a5145fe5efa4201e7210b4ec5b416ba71724784fb2eb3d33a08f92b58b8b";
	$api_url = "http://checkurl.phishtank.com/checkurl/";
	//$url =  base64_encode("http://nakinaindustries.com");

	$curl = curl_init();    
	curl_setopt( $curl, CURLOPT_URL, $api_url ); 
	curl_setopt( $curl, CURLOPT_RETURNTRANSFER, TRUE );
	curl_setopt( $curl, CURLOPT_CUSTOMREQUEST, 'POST' );
	curl_setopt( $curl, CURLOPT_POSTFIELDS, array(
		"url" => base64_encode( $url ),
		"format" => 'json',
		"app_key" => $api_key
	));


	$curl_response = curl_exec($curl);
	curl_close($curl);

	//echo $curl_response; 
	$response = json_decode($curl_response , true);
	return $response ;
}

function sucuricurl($url) {
	// Assigning cURL options to an array
	$options = Array(
		CURLOPT_RETURNTRANSFER => TRUE,  // Setting cURL's option to return the webpage data
		CURLOPT_FOLLOWLOCATION => TRUE,  // Setting cURL to follow 'location' HTTP headers
		CURLOPT_AUTOREFERER => TRUE, // Automatically set the referer where following 'location' HTTP headers
		CURLOPT_CONNECTTIMEOUT => 120,   // Setting the amount of time (in seconds) before the request times out
		CURLOPT_TIMEOUT => 120,  // Setting the maximum amount of time for cURL to execute queries
		CURLOPT_MAXREDIRS => 10, // Setting the maximum number of redirections to follow
		CURLOPT_USERAGENT => $_SERVER['HTTP_USER_AGENT'], //"Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.9.1a2pre) Gecko/2008073000 Shredder/3.0a2pre ThunderBrowse/3.2.1.8",  // Setting the useragent
		CURLOPT_URL => $url, // Setting cURL's URL option with the $url variable passed into the function
		CURLOPT_SSL_VERIFYPEER => FALSE
	);
	 
	$ch = curl_init();  // Initialising cURL 
	curl_setopt_array($ch, $options);   // Setting cURL's options using the previously assigned array data in $options
	$data = curl_exec($ch); // Executing the cURL request and assigning the returned data to the $data variable
	curl_close($ch);    // Closing cURL 
	return $data;   // Returning the data from the function 
}

function scrape_between($data, $start, $end){
	$data = stristr($data, $start); // Stripping all data from before $start
	$data = substr($data, strlen($start));  // Stripping $start
	$stop = stripos($data, $end);   // Getting the position of the $end of the data to scrape
	$data = substr($data, 0, $stop);    // Stripping all data from after and including the $end of the data to scrape
	return $data;   // Returning the scraped data from the function
}

function sucuriCheck($url){
	
	//$theUrl = "https://sitecheck.sucuri.net/results/goooogleadsence.biz";
	$theUrl = "https://sitecheck.sucuri.net/results/" . $url;
	$results_page = sucuricurl($theUrl); // Downloading the results page using our curl() funtion
     
    $results_page = scrape_between($results_page, "<table class=\"table main-result\">", "</table>"); // Scraping out only the middle section of the results page that contains our results
     
    $separate_results = explode("<tr>", $results_page);   // Expploding the results into separate parts into an array
         
	$status = scrape_between($separate_results[3], "<td class=\"red\">", "</td>");
	

	return $status;
	
}

function virustotalCheck($url){
	$api_key = getenv('VT_API_KEY') ? getenv('VT_API_KEY') :'80eb4a2c6fc02ca6194349f6add36b6ee8771febe8de6b9792062d6bc1d0f495';
	$post = array('apikey' => $api_key,'resource'=> $url, 'scan' => 1 );
	
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, 'https://www.virustotal.com/vtapi/v2/url/report');
	curl_setopt($ch, CURLOPT_POST, True);
	curl_setopt($ch, CURLOPT_VERBOSE, 1); // remove this if your not debugging
	curl_setopt($ch, CURLOPT_ENCODING, 'gzip,deflate'); // please compress data
	curl_setopt($ch, CURLOPT_USERAGENT, "gzip, My php curl client");
	curl_setopt($ch, CURLOPT_RETURNTRANSFER ,True);
	curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
	 
	$result=curl_exec ($ch);
	$status_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
	//print("status = $status_code\n");
	
	if ($status_code == 200) { // OK
	  $js = json_decode($result, true);
	  
		return $js;
		
	} else {  // Error occured

	  return $result;
	}
	curl_close ($ch);
}

//add_hook( "AdminHomepage", 1, "bsc_check_update" );

add_hook( "DailyCronJob", 1, "bezasitechecker_DailyCronJob" );
//add_hook( "PostAutomationTask", 1, "bezasitechecker_PostAutomationTask" );
//add_hook( "TicketUserReply", 1, "bezasitechecker_ticket_reply" );