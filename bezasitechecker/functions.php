<?php
/**
 * WHMCS BEZA Site Checker Functions
 *
 * Use this addon to use Google Safebrowsing API, Phishtank API and Sucuri
 *
 * @package    WHMCS 5.2.1+
 * @author     BEZA
 * @copyright  Copyright (c) BEZA 2017-2018
 * @license    GPL v3+
 * @version    1.0.1
 * @link       BEZA.NET
 */

if (!defined("WHMCS"))
	die("This file cannot be accessed directly");

function bsc_log($description, $request, $response){
	logModuleCall('bezasitechecker', $description, $request, $response);
}

function bsc_debug($debugdata, $debugdata2){
	logModuleCall('bezasitechecker','debug', $debugdata, $debugdata2);
}

function bsc_get_url ( $ssl = true ) {
	global $CONFIG;
	$ssl_url = $CONFIG['SystemURL'];
	$nonssl_url = $CONFIG['SystemSSLURL'];
	if(!$ssl_url){
		return $nonssl_url;
	} else {
		return $ssl_url;
	}

}

function bsc_get_customadminpath() {
	return $GLOBALS['customadminpath'];
}

function bsc_get_admin_url(){
	$customadminpath = bsc_get_customadminpath();
	if (!$customadminpath) $customadminpath = "admin";
	$whmcsurl = bsc_get_url();

	return $whmcsurl."/".$customadminpath;
}

function bsc_get_admin_ticket_url($ticketid){
	if(bsc_get_enable_mobile()){
		$ticket_url = bsc_get_url() . '/mobile/tickets.php?action=view&id=' . $ticketid;
	} else {
		$ticket_url = bsc_get_admin_url() . '/supporttickets.php?action=viewticket&id=' . $ticketid;
	}
	return $ticket_url;
}

function bsc_get_userkey(){
    $sql = mysql_query("SELECT value FROM tbladdonmodules WHERE module='bezasitechecker' AND setting='userkey'");
        $result = mysql_fetch_array($sql);
        if ($result['value']){
                return $result['value'];
        } else {
                bsc_log('userkey_error', $sql, $result);
                return false;
        }
}
function bsc_get_enable_mobile(){
    $sql = mysql_query("SELECT value FROM tbladdonmodules WHERE module='bezasitechecker' AND setting='enable_mobile'");
        $result = mysql_fetch_array($sql);
        if ($result['value']){
                return $result['value'];
        } else {
                return false;
        }
}
function bsc_get_enable_google(){
    $sql = mysql_query("SELECT value FROM tbladdonmodules WHERE module='bezasitechecker' AND setting='enable_google'");
        $result = mysql_fetch_array($sql);
        if ($result['value']){
                return $result['value'];
        } else {
                return false;
        }
}
function bsc_get_enable_phishtank(){
    $sql = mysql_query("SELECT value FROM tbladdonmodules WHERE module='bezasitechecker' AND setting='enable_phishtank'");
        $result = mysql_fetch_array($sql);
        if ($result['value']){
                return $result['value'];
        } else {
                return false;
        }
}
function bsc_get_enable_sucuri(){
    $sql = mysql_query("SELECT value FROM tbladdonmodules WHERE module='bezasitechecker' AND setting='enable_sucuri'");
        $result = mysql_fetch_array($sql);
        if ($result['value']){
                return $result['value'];
        } else {
                return false;
        }
}
function bsc_get_enable_virustotal(){
    $sql = mysql_query("SELECT value FROM tbladdonmodules WHERE module='bezasitechecker' AND setting='enable_virustotal'");
        $result = mysql_fetch_array($sql);
        if ($result['value']){
                return $result['value'];
        } else {
                return false;
        }
}